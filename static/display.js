function filter_array(test_array) {
    var index = -1,
        arr_length = test_array ? test_array.length : 0,
        resIndex = -1,
        result = [];

    while (++index < arr_length) {
        var value = test_array[index];

        if (value) {
            result[++resIndex] = value;
        }
    }

    return result;
}

function encrypt(msgString, key) {
    // msgString is expected to be Utf8 encoded
    var iv = CryptoJS.lib.WordArray.random(16);
    var encrypted = CryptoJS.AES.encrypt(msgString, key, {
        iv: iv
    });
    console.log('iv: ' + iv.toString(CryptoJS.enc.Base64));
    console.log('encrypted: ' + encrypted.ciphertext.toString(CryptoJS.enc.Base64));
    return iv.concat(encrypted.ciphertext).toString(CryptoJS.enc.Base64);
}

function decrypt(ciphertextStr, key) {
    var ciphertext = CryptoJS.enc.Base64.parse(ciphertextStr);

    // split IV and ciphertext
    var iv = ciphertext.clone();
    iv.sigBytes = 16;
    iv.clamp();
    console.log('iv: ' + iv.toString(CryptoJS.enc.Base64));
    ciphertext.words.splice(0, 4); // delete 4 words = 16 bytes
    ciphertext.sigBytes -= 16;
    console.log('encrypted: ' + ciphertext.toString(CryptoJS.enc.Base64));

    // decryption
    var decrypted = CryptoJS.AES.decrypt({ ciphertext: ciphertext }, key, {
        iv: iv
    });
    return decrypted.toString(CryptoJS.enc.Utf8);
}


// JavaScript source code
$(function () {
    $('#decBtn').click(function () {
        var data = $('form').serializeJSON();
        var enckey = CryptoJS.enc.Utf8.parse(CryptoJS.MD5(data.encryptPassword).toString());
        console.log(data);
        $.get(
            "/retrieve",
            {},
            function (response) {
                var error = false;
                try {
                    var plaintext = decrypt(response,enckey);
                    console.log(plaintext);
                }
                catch (err) {
                    alert('Wrong Decryption Key!');
                    error = True;
                }
                if ((!error) && (plaintext!='') ) {
                    var list = JSON.parse(plaintext);
                    list = filter_array(list);
                    var stringified = JSON.stringify(list);
                    console.log('Hasil Hilang Null: ', stringified);
                    var cols = [];

                    for (var i = 0; i < list.length; i++) {
                        for (var k in list[i]) {
                            if (cols.indexOf(k) === -1) {

                                // Push all keys to the array 
                                cols.push(k);
                            }
                        }
                    }

                    // Create a table element 
                    var table = document.createElement("table");
                    table.setAttribute("width", "300");
                    // Create table row tr element of a table 
                    var tr = table.insertRow(-1);

                    for (var i = 0; i < cols.length; i++) {

                        // Create the table header th element 
                        var theader = document.createElement("th");
                        theader.innerHTML = cols[i];

                        // Append columnName to the table row 
                        tr.appendChild(theader);
                    }

                    // Adding the data to the table 
                    for (var i = 0; i < list.length; i++) {

                        // Create a new row 
                        trow = table.insertRow(-1);
                        for (var j = 0; j < cols.length; j++) {
                            var cell = trow.insertCell(-1);

                            // Inserting the cell at particular place 
                            cell.innerHTML = list[i][cols[j]];
                        }
                    }

                    // Add the newely created table containing json data 
                    var el = document.getElementById("table");
                    el.innerHTML = "";
                    el.appendChild(table); 
                }
            });

    });
});

// JavaScript source code
$(function () {
    $('#delBtn').click(function () {
        var data = $('form').serializeJSON();
        var enckey = CryptoJS.enc.Utf8.parse(CryptoJS.MD5(data.encryptPassword).toString());
        console.log(data);
        $.get(
            "/retrieve",
            {},
            function (response) {
                var error = false;
                try {
                    var plaintext = decrypt(response, enckey);
                    console.log(plaintext);
                }
                catch (err) {
                    alert('Wrong Decryption Key!');
                    error = True;
                }
                if ((!error) && (plaintext != '')) {
                    var list = JSON.parse(plaintext);
                    list = filter_array(list);
                    
                }
                var found = false;
                for (let i = 0; i < list.length; i++) {
                    if (list[i].servicename == data.serviceName) {
                        found = true;
                        delete list[i];
                        list = filter_array(list);
                        var stringified = JSON.stringify(list);
                        console.log(stringified);
                        var ciphertext = encrypt(stringified, enckey);
                        var sendObj = { data: ciphertext.toString() };
                        console.log(sendObj);
                        $.ajax({
                            type: "POST",
                            url: '/update',
                            data: sendObj,
                            success: function (response) {
                                alert('Password for ' + data.serviceName + ' has been deleted successfully!'); 
                            }
                        });

                    }
                }
                if (found == false) {
                    alert(data.serviceName + ' not found!');
                }
            });
        

    });
});

$(function () {
    $('#chgBtn').click(function () {
        var data = $('form').serializeJSON();
        var enckey = CryptoJS.enc.Utf8.parse(CryptoJS.MD5(data.encryptPassword).toString());
        var newEncKey = CryptoJS.enc.Utf8.parse(CryptoJS.MD5(data.newEncryptPassword).toString());
        console.log(data);
        $.get(
            "/retrieve",
            {},
            function (response) {
                var error = false;
                try {
                    var plaintext = decrypt(response, enckey);
                    console.log(plaintext);
                }
                catch (err) {
                    alert('Wrong Decryption Key!');
                    error = True;
                }
                if ((!error) && (plaintext != '')) {
                    var list = JSON.parse(plaintext);
                    list = filter_array(list);
                    var stringified = JSON.stringify(list);
                    console.log(stringified);
                    var ciphertext = encrypt(stringified, newEncKey);
                    var sendObj = { data: ciphertext.toString() };
                    console.log(sendObj);
                    $.ajax({
                        type: "POST",
                        url: '/update',
                        data: sendObj,
                        success: function (response) {
                            alert('Password changed successfully!');
                        }
                    });
                }
                else {
                    alert('Wrong Decryption Key!');
                }
            });


    });
});