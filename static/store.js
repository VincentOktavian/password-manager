(function ($) {
    $.fn.serializeFormJSON = function () {

        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
})(jQuery);


function filter_array(test_array) {
    var index = -1,
        arr_length = test_array ? test_array.length : 0,
        resIndex = -1,
        result = [];

    while (++index < arr_length) {
        var value = test_array[index];

        if (value) {
            result[++resIndex] = value;
        }
    }

    return result;
}

//var key = CryptoJS.enc.Utf8.parse('1234567890123456'); // TODO change to something with more entropy

function encrypt(msgString, key) {
    // msgString is expected to be Utf8 encoded
    var iv = CryptoJS.lib.WordArray.random(16);
    var encrypted = CryptoJS.AES.encrypt(msgString, key, {
        iv: iv
    });
    console.log('iv: ' + iv.toString(CryptoJS.enc.Base64));
    console.log('encrypted: ' + encrypted.ciphertext.toString(CryptoJS.enc.Base64));
    return iv.concat(encrypted.ciphertext).toString(CryptoJS.enc.Base64);
}

function decrypt(ciphertextStr, key) {
    var ciphertext = CryptoJS.enc.Base64.parse(ciphertextStr);

    // split IV and ciphertext
    var iv = ciphertext.clone();
    iv.sigBytes = 16;
    iv.clamp();
    console.log('iv: ' + iv.toString(CryptoJS.enc.Base64));
    ciphertext.words.splice(0, 4); // delete 4 words = 16 bytes
    ciphertext.sigBytes -= 16;
    console.log('encrypted: ' + ciphertext.toString(CryptoJS.enc.Base64));

    // decryption
    var decrypted = CryptoJS.AES.decrypt({ ciphertext: ciphertext }, key, {
        iv: iv
    });
    return decrypted.toString(CryptoJS.enc.Utf8);
}

$(function () {
    $('#addBtn').click(function () {
        //var user = $('#inputUsername').val();
        //var pass = $('#inputPassword').val();
        var data = $('form').serializeJSON();
        console.log(data);
        if (data.password == data.validatePassword) {
            delete data.validatePassword;
            var enckey = CryptoJS.enc.Utf8.parse(CryptoJS.MD5(data.encryptPassword).toString()); 
            console.log(data);
            console.log('MD5 Hex: ' + enckey + 'type: '+ typeof(enckey));

            delete data.encryptPassword;
            $.get(
                "/retrieve",
                {},
                function (response) {
                    if (response == null) {
                        console.log('page content: null' + response);
                        var arrayofObj = [];
                        arrayofObj.push(data);
                        console.log(arrayofObj);
                        var stringified = JSON.stringify(arrayofObj);
                        console.log(stringified);
                        var ciphertext = encrypt(stringified, enckey);
                        var sendObj = { data: ciphertext.toString() };

                        console.log(sendObj);
                        $.ajax({
                            type: "POST",
                            url: '/update',
                            data: sendObj,
                            success: function (response) {
                                alert('Successfully Added!');
                            }
                        });

                    }
                    else {
                        console.log('page content: ' + response);
                        //var bytes = CryptoJS.AES.decrypt(response, enckey);
                        var error = false;
                        try {
                            //var plaintext = bytes.toString(CryptoJS.enc.Utf8);
                            var plaintext = decrypt(response, enckey);
                            console.log('Decrypted: '+plaintext);
                        }
                        catch (err) {
                            
                            error = true;
                        }
                        if ((!error)&&(plaintext!='')) {
                            console.log('Correct Key');
                            var obj = JSON.parse(plaintext);
                            obj = filter_array(obj);
                            var found = false;
                            for (var j = 0; j < obj.length; j++) {
                                console.log(obj[j]);
                                try {
                                    if (obj[j].servicename == data.servicename) {
                                        found = true;
                                        var r = confirm(data.servicename + " is existing, do you want to replace?");
                                        if (r == true) {
                                            console.log('replace');
                                            obj[j] = data;
                                            var stringified = JSON.stringify(obj);
                                            console.log(stringified);
                                            var ciphertext = encrypt(stringified, enckey);
                                            var sendObj = { data: ciphertext.toString() };

                                            console.log(sendObj);
                                            $.ajax({
                                                type: "POST",
                                                url: '/update',
                                                data: sendObj,
                                                success: function (response) {
                                                    alert('Successfully Replaced!');
                                                }
                                            });
                                            return false;

                                        } else {
                                            console.log('do nothing');
                                            return false;
                                        }
                                    }
                                }
                                catch(err){
                                    console.log(err);
                                }

                            }
                            if (found == false) {
                                obj.push(data);
                                var stringified = JSON.stringify(obj);
                                console.log(stringified);
                                var ciphertext = encrypt(stringified, enckey);
                                var sendObj = { data: ciphertext.toString() };

                                console.log(sendObj);
                                $.ajax({
                                    type: "POST",
                                    url: '/update',
                                    data: sendObj,
                                    success: function (response) {
                                        alert('Successfully Added!');
                                    }
                                });
                            }
                            

                        }
                        else {
                            alert('Wrong Encryption Key!');
                        }

                    }
                }
            );
        }
        else {
            alert("Validation password doesn't match!");
        }
        
        //$.ajax({
        //    url: '/signUpUser',
        //    data: $('form').serialize(),
        //    type: 'POST',
        //    success: function (response) {
        //        console.log(response);
        //    },
        //    error: function (error) {
        //        console.log(error);
        //    }
        //});
    });
});
